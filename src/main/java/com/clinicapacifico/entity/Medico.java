package com.clinicapacifico.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Medico extends Persona {

	@Column(length = 5, unique = true)
	private String colegiatura;

	@OneToMany(mappedBy = "medico", fetch = FetchType.LAZY)
	private List<HorarioMedico> horarios;
	
	@OneToMany(mappedBy = "medico" , fetch = FetchType.LAZY)
	private List<CitaMedica> citas; 

	public Medico() {

	}

	public String getColegiatura() {
		return colegiatura;
	}

	public void setColegiatura(String colegiatura) {
		this.colegiatura = colegiatura;
	}

	public List<HorarioMedico> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<HorarioMedico> horarios) {
		this.horarios = horarios;
	}

	public List<CitaMedica> getCitas() {
		return citas;
	}

	public void setCitas(List<CitaMedica> citas) {
		this.citas = citas;
	}


	

}
