package com.clinicapacifico.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "examen_medico_id", "paciente_id", "fecha" }))
@Entity
public class ExamenMedicoPaciente {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "examen_medico_id")
	private ExamenMedico examen_medico;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "paciente_id")
	private Paciente paciente;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha")
	private Date fecha;

	public ExamenMedicoPaciente() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ExamenMedico getExamen_medico() {
		return examen_medico;
	}

	public void setExamen_medico(ExamenMedico examen_medico) {
		this.examen_medico = examen_medico;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
