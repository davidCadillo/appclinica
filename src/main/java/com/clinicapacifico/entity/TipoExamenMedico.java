package com.clinicapacifico.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TipoExamenMedico {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(length = 25)
	private String tipo;

	@OneToMany(mappedBy = "tipoExamen", fetch = FetchType.LAZY)
	private List<ExamenMedico> examenes;

	public TipoExamenMedico() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<ExamenMedico> getExamenes() {
		return examenes;
	}

	public void setExamenes(List<ExamenMedico> examenes) {
		this.examenes = examenes;
	}

}
