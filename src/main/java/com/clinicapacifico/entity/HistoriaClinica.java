package com.clinicapacifico.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class HistoriaClinica {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@OneToMany(mappedBy="historiaClinica", fetch=FetchType.LAZY)
	private List<Paciente> pacientes;
	
	public HistoriaClinica(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}
	
	
	
	
	
	
	
	
	
	
	

}
