package com.clinicapacifico.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class ExamenMedico {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(length = 25)
	@NotNull
	private String nombre;

	@NotNull
	private double precio;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tipo_examen_id")
	@NotNull
	private TipoExamenMedico tipoExamen;

	@NotNull
	@OneToMany(mappedBy = "examen_medico")
	private List<ExamenMedicoPaciente> examenes;

	public ExamenMedico() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public TipoExamenMedico getTipoExamen() {
		return tipoExamen;
	}

	public void setTipoExamen(TipoExamenMedico tipoExamen) {
		this.tipoExamen = tipoExamen;
	}

	public List<ExamenMedicoPaciente> getExamenes() {
		return examenes;
	}

	public void setExamenes(List<ExamenMedicoPaciente> examenes) {
		this.examenes = examenes;
	}
	
	

}
