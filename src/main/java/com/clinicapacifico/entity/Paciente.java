package com.clinicapacifico.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Paciente extends Persona {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "estado_civil_id")
	@NotNull
	private EstadoCivil estadoCivil;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seguro_salud_id")
	private SeguroSalud seguroSalud;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "grado_instruccion_id")
	@NotNull
	private GradoInstruccion gradoInstruccion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "empresa_id")
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tipo_paciente_id")
	@NotNull
	private TipoPaciente tipoPaciente;

	@NotNull
	@OneToMany(mappedBy = "paciente")
	private List<ExamenMedicoPaciente> examenes;

	@NotNull
	@OneToMany(mappedBy = "paciente", fetch = FetchType.LAZY)
	private List<CitaMedica> citas;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "historia_clinica_id")
	private HistoriaClinica historiaClinica;

	private Paciente() {

	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public SeguroSalud getSeguroSalud() {
		return seguroSalud;
	}

	public void setSeguroSalud(SeguroSalud seguroSalud) {
		this.seguroSalud = seguroSalud;
	}

	public GradoInstruccion getGradoInstruccion() {
		return gradoInstruccion;
	}

	public void setGradoInstruccion(GradoInstruccion gradoInstruccion) {
		this.gradoInstruccion = gradoInstruccion;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public TipoPaciente getTipoPaciente() {
		return tipoPaciente;
	}

	public void setTipoPaciente(TipoPaciente tipoPaciente) {
		this.tipoPaciente = tipoPaciente;
	}

	public List<ExamenMedicoPaciente> getExamenes() {
		return examenes;
	}

	public void setExamenes(List<ExamenMedicoPaciente> examenes) {
		this.examenes = examenes;
	}

	public List<CitaMedica> getCitas() {
		return citas;
	}

	public void setCitas(List<CitaMedica> citas) {
		this.citas = citas;
	}

	public HistoriaClinica getHistoriaClinica() {
		return historiaClinica;
	}

	public void setHistoriaClinica(HistoriaClinica historiaClinica) {
		this.historiaClinica = historiaClinica;
	}

}
