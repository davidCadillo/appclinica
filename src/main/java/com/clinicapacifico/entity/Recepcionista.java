package com.clinicapacifico.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Recepcionista extends Persona {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "turno_id")
	@NotNull
	private Turno turno;
	
	@OneToMany(mappedBy="recepcionista" , fetch=FetchType.LAZY)
	private List<CitaMedica> citas;

	public Recepcionista() {

	}

	public Turno getHorario() {
		return turno;
	}

	public void setHorario(Turno turno) {
		this.turno = turno;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public List<CitaMedica> getCitas() {
		return citas;
	}

	public void setCitas(List<CitaMedica> citas) {
		this.citas = citas;
	}
	
	

}
