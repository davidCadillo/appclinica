package com.clinicapacifico.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.clinicapacifico.entity.Sexo;

@Repository
@Transactional
public interface SexoRepository extends JpaRepository<Sexo, Integer> {

}
