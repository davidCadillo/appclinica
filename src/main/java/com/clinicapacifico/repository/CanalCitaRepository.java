package com.clinicapacifico.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.clinicapacifico.entity.CanalCita;

@Repository
@Transactional
public interface CanalCitaRepository extends JpaRepository<CanalCita, Integer> {

}
