package com.clinicapacifico;

import org.springframework.boot.CommandLineRunner;
import java.util.List;
import java.util.ArrayList;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.clinicapacifico.entity.Turno;
import com.clinicapacifico.entity.CanalCita;
import com.clinicapacifico.entity.Sexo;
import com.clinicapacifico.repository.CanalCitaRepository;
import com.clinicapacifico.repository.SexoRepository;
import com.clinicapacifico.repository.TurnoRepository;

@SpringBootApplication
public class AppclinicaApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AppclinicaApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(AppclinicaApplication.class, args);
	}

	@Bean
	public CommandLineRunner insertarTurnos(TurnoRepository turnoRepository) {
		return (args) -> {

			Turno turnoManiana = new Turno();
			turnoManiana.setTurno("MAÑANA");

			Turno turnoTarde = new Turno();
			turnoTarde.setTurno("TARDE");

			Turno turnoNoche = new Turno();
			turnoNoche.setTurno("NOCHE");

			turnoRepository.save(turnoManiana);
			turnoRepository.save(turnoTarde);
			turnoRepository.save(turnoNoche);
		};
	}

	@Bean
	public CommandLineRunner insertarCanalesCitas(CanalCitaRepository canalCitaRepository) {

		List<CanalCita> canalesCitas = new ArrayList<CanalCita>();
		CanalCita presencial = new CanalCita();
		presencial.setCanal("PRESENCIAL");
		CanalCita telefono = new CanalCita();
		telefono.setCanal("TELEFONO");
		CanalCita web = new CanalCita();
		web.setCanal("WEB");
		CanalCita movil = new CanalCita();
		movil.setCanal("MOVIL");
		canalesCitas.add(presencial);
		canalesCitas.add(telefono);
		canalesCitas.add(web);
		canalesCitas.add(movil);

		canalCitaRepository.save(canalesCitas);
		return (args) -> {

		};

	}

	@Bean
	public CommandLineRunner insertarSexos(SexoRepository sexoRepository) {

		return (args) -> {

			Sexo masculino = new Sexo();
			masculino.setSexo("MASCULINO");

			Sexo femenino = new Sexo();
			femenino.setSexo("FEMENINO");
			
			sexoRepository.save(masculino);
			sexoRepository.save(femenino);
		};
	}

}
